package com.nacarseven.celebrate.data.entities;

import java.util.List;

/**
 * Created by nacarseven on 18/06/17.
 */

public class User {

    private long id;
    private String name;
    private String imageUrl;
    private List<Friend> friends;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }
}
