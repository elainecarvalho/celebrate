package com.nacarseven.celebrate.data.local;

import com.nacarseven.celebrate.CelebrateApplication;
import com.nacarseven.celebrate.data.entities.User;
import com.nacarseven.celebrate.models.Session;

/**
 * Created by nacarseven on 18/06/17.
 */

public class InMemoryHelper {

    private CelebrateApplication application;



    public InMemoryHelper() {

        application = CelebrateApplication.getInstance();
    }

    public Session getSession() {
        Session session = application.getSession();

        if (session == null) {
            application.setSession(session);
        }

        return session;
    }

    public void setSession(Session session) {
        application.setSession(session);
    }

    public User getLoggedInUser() {
        User user = application.getLoggedInUser();
        return user;
    }

    public void setLoggedInUser(User user) {
        application.setLoggedInUser(user);
    }

    public void clearLoginData() {
        application.setSession(null);
        application.setLoggedInUser(null);
    }

    public long getLoggedInUserId() {
        User user = getLoggedInUser();
        return user != null ? user.getId() : 0;
    }

}
