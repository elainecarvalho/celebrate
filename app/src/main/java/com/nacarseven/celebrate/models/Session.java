package com.nacarseven.celebrate.models;

/**
 * Created by nacarseven on 18/06/17.
 */

public class Session {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
