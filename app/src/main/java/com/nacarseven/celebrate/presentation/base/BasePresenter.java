package com.nacarseven.celebrate.presentation.base;

/**
 * Created by nacarseven on 18/06/17.
 */

public interface BasePresenter {

    void detachView();
}
