package com.nacarseven.celebrate.presentation.login;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.google.gson.JsonObject;
import com.nacarseven.celebrate.data.entities.User;
import com.nacarseven.celebrate.data.local.InMemoryHelper;
import com.nacarseven.celebrate.domain.LoginInteractor;
import com.nacarseven.celebrate.models.Session;
import com.nacarseven.celebrate.presentation.base.BasePresenter;

import org.json.JSONArray;
import org.json.JSONObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by nacarseven on 18/06/17.
 */

 class LoginPresenter implements BasePresenter {

    private LoginView view;
    private LoginInteractor interactor;
    private InMemoryHelper inMemoryHelper;


     LoginPresenter(LoginView view) {
        this.view = view;
        inMemoryHelper = new InMemoryHelper();
    }

    void setSession(String token, long userId) {
        Session session = new Session();
        session.setToken(token);
        User user = new User();
        user.setId(userId);
        inMemoryHelper.setSession(session);
        inMemoryHelper.setLoggedInUser(user);
        interactor = new LoginInteractor();
        getFriends();
    }


    void getFriends() {
        if (inMemoryHelper.getSession() != null) {
            interactor.getFriend()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<JsonObject>() {
                        @Override
                        public void call(JsonObject jsonObject) {

                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {

                        }
                    });
        }


    }

     void requestFacebookFriends(AccessToken accessToken) {
        GraphRequestBatch batch = new GraphRequestBatch(
                GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                    }
                }),
                GraphRequest.newMyFriendsRequest(accessToken, new GraphRequest.GraphJSONArrayCallback() {
                    @Override
                    public void onCompleted(JSONArray objects, GraphResponse response) {

                    }
                })
        );
        batch.executeAsync();
    }

    @Override
    public void detachView() {

    }
}
