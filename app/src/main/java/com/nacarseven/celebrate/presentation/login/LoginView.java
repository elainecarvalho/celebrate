package com.nacarseven.celebrate.presentation.login;

import com.nacarseven.celebrate.data.entities.Friend;

import java.util.List;

/**
 * Created by nacarseven on 18/06/17.
 */

 interface LoginView {

    void loadFriends(List<Friend> friends);
}
