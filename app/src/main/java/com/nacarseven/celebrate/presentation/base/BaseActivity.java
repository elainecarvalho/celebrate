package com.nacarseven.celebrate.presentation.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by nacarseven on 18/06/17.
 */

public abstract class BaseActivity extends AppCompatActivity {


    //region LIFECYCLES
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

}
