package com.nacarseven.celebrate.network.services;

import com.google.gson.JsonObject;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by nacarseven on 17/06/17.
 */

public interface Service {



    @GET("v2.9/{user-id}/friends")
    Observable<JsonObject> getFriends(@Path("user-id") long userId);

    @GET("v2.3/{user-id}/taggable_friends")
    Observable<JsonObject> getFriendList(@Path("user-id") long userId);

    @GET("/v2.3/{user-id}/events")
    Observable<JsonObject> getEvents(@Path("user-id") long userId);
}
