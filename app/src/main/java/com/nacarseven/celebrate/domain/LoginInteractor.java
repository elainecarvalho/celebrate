package com.nacarseven.celebrate.domain;

import com.google.gson.JsonObject;
import com.nacarseven.celebrate.BuildConfig;
import com.nacarseven.celebrate.data.local.InMemoryHelper;
import com.nacarseven.celebrate.network.ServiceGenerator;
import com.nacarseven.celebrate.network.services.Service;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by nacarseven on 18/06/17.
 */

public class LoginInteractor {

    private InMemoryHelper inMemoryHelper;
    private Service service;

    public LoginInteractor() {
        inMemoryHelper = new InMemoryHelper();
        service = ServiceGenerator.createService(Service.class, BuildConfig.API_URL, inMemoryHelper.getSession().getToken());

    }

    public Observable<JsonObject> getFriend(){
        return service.getFriendList(inMemoryHelper.getLoggedInUserId())
                .map(new Func1<JsonObject, JsonObject>() {
                    @Override
                    public JsonObject call(JsonObject jsonObject) {
                        return jsonObject;
                    }
                });
    }


}
